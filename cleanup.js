const twilio     = require('twilio');
const config     = require('./config');

// ============================================
// ============================================
// ====== RESET THE CONVERSATIONS==============
// ============================================
// ============================================

let client = new twilio(config.twilio.accountSid, config.twilio.authToken);

client.conversations.v1.conversations
.list({limit: 100})
.then(conversations => {
    conversations.forEach(c => {
        console.log(`Deleting conversation: ${c.sid}`);
        client.conversations.conversations(c.sid)
        .remove();

    })
});
