const config     = require('./config');
const express    = require('express');
const bodyParser = require('body-parser');
const twilio     = require('twilio');
const ngrok      = require('ngrok');

const app = new express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.post('/token/:identity', (request, response) => {
  //console.log("inside token post 0");
  //console.log(config.twilio.accountSid);
  //console.log(config.twilio.apiKey);
  //console.log(config.twilio.apiSecret);
  const identity = request.params.identity;
  const accessToken = new twilio.jwt.AccessToken(config.twilio.accountSid, config.twilio.apiKey, config.twilio.apiSecret);
  const chatGrant = new twilio.jwt.AccessToken.ChatGrant({
    serviceSid: config.twilio.chatServiceSid,
  });
  accessToken.addGrant(chatGrant);
  accessToken.identity = identity;
  console.log("inside token post "+accessToken.identity);
  response.set('Content-Type', 'application/json');
  response.send(JSON.stringify({
    token: accessToken.toJwt(),
    identity: identity
  }));
})

app.listen(config.port, () => {
  console.log(`Application started at localhost:${config.port}`);
});


// ============================================
// ============================================
// ====== HANDLE NEW-CONVERSATION HOOK ========
// ============================================
// ============================================

let client = new twilio(config.twilio.accountSid, config.twilio.authToken);

//Support  service chat side


const isParticipant = async (  //returns bool if participant name found
  _convSid,
  __name
) => {

let participants;
  try{  
    participants = await client.conversations.v1.conversations(_convSid).participants.list({limit:20}) //up to twenty
  
    participants.forEach (p =>{
      if(p.identity===__name)
       return true;
    });
  }
  catch(err)
  {
      console.log(`Failed to get all conversations!`, err);
      return false; //we will simply allow to continue checking
  }
 return false;
};

const isNew = async (_name) => {  //return boolean to inform if chaf user already connected to Support
  let conversations;
  try{
    conversations = await client.conversations.v1.conversations.list({limit: 20}); //max num of conversation that can be shown at once
    //return conversations;
    conversations.forEach(c => {
      if(isParticipant(c,_name))
        return false; 
     });
    
  }
  catch(err){
    console.log(`Failed to get all conversations!`, err);
    return true; //in case of error we will assume it is a new conversation opening not to lose the exchanges
  }
  return true;
  
};



app.post('/open-support/:name', (req, res) => {
  console.log(`Support conversation`);
  //if not Support create new conversation 
  const name = req.params.name;
  
  if (name !== "Support"){

    const name = req.params.name;
   
    let newUser = isNew(name);

    console.log(`new users: ${newUser} `);
    if(newUser){
      client.conversations.v1.conversations.create({
        messagingServiceSid: 'MG04ed6cdb573097f3126e32d08aaaebe4', //hardcoded to the message service of Support
        friendlyName: name
      })
      .then(conversation => {
        console.log(`Added conversation  for ${name}.`);
  
        client.conversations.v1.conversations(conversation.sid)
        .participants
        .create({
            identity: name
         })
        .then(participant => console.log(`Added this participant: ${participant.identity} to ${participant.conversationSid}.`))
        .catch(err => console.error(`Failed to add a member to ${conversation.sid}!`, err));
  
      })
      .catch(err => console.error(`Failed to create conversation for ${name}!`, err));
    }
  }
  else{
    console.log ("it is the Support user, does not create conversations");
  }
  res.sendStatus(200);

});
  

app.post('/conversations-post', (req, res) => {
  
  if (req.body.EventType === 'onConversationAdded') {
    console.log("Received a webhook:", req.body);
    console.log(`onConversationAdded`);
    const me = "Support";
    client.conversations.v1.conversations(req.body.ConversationSid)
      .participants
      .create({
          identity: me
        })
      .then(participant => console.log(`Added ${participant.identity} to ${req.body.ConversationSid}.`))
      .catch(err => console.error(`Failed to add a member to ${req.body.ConversationSid}!`, err));
  }
  if (req.body.EventType === 'onMessageAdded') {
    console.log(`onMessageAdded`);
    //code
  }
  
  if (req.body.EventType === 'onParticipantAdded') {
    console.log(`onParticipantAdded`);
    //code
  }
 
  if (req.body.EventType === 'onConversationRemoved') {
    console.log(`ConversationRemoved`);
    //code
  }
  if (req.body.EventType === 'onMessageRemoved') {
    console.log(`onMessageRemoved`);
    //code
  }
  if (req.body.EventType === 'onParticipantRemoved') {
    console.log(`onParticipantRemoved`);
    //code
  }

  if (req.body.EventType === 'onConversationUpdated') {
    console.log(`onConversationUpdated`);
    //code
  }
  if (req.body.EventType === 'onMessageUpdated') {
    console.log(`onMessageUpdated`);
    //code
  }
  if (req.body.EventType === 'onParticipantUpdated') {
    console.log(`onParticipantUpdated`);
    //code
  }

  console.log("(200 OK!)");
  res.sendStatus(200);
});

app.post('/outbound-status', (req, res) => {
  console.log(`Message ${req.body.SmsSid} to ${req.body.To} is ${req.body.MessageStatus}`);
  res.sendStatus(200);
})

app.post('/conversations-pre', (req, res) => {
  //console.log("Received a pre webhook:", req.body);
  if (req.body.EventType === 'onConversationAdd') {
    console.log(`onConversatioAdd`);

  }
   if (req.body.EventType === 'onMessageAdd') {
    console.log(`onMessageAdd`);

  }
  
  if (req.body.EventType === 'onParticipantAdd') {
    console.log(`onParticipantAdd`);
    //code
  }
 
  if (req.body.EventType === 'onConversationRemove') {
    console.log(`ConversationRemove`);
    //code
  }
  if (req.body.EventType === 'onMessageRemove') {
    console.log(`onMessageRemove`);
    //code
  }
  if (req.body.EventType === 'onParticipantRemove') {
    console.log(`onParticipantRemove`);
    //code
  }

  if (req.body.EventType === 'onConversationUpdate') {
    console.log(`onConversationUpdate`);
    //code
  }
  if (req.body.EventType === 'onMessageUpdate') {
    console.log(`onMessageUpdate`);
    //code
  }
  if (req.body.EventType === 'onParticipantUpdate') {
    console.log(`onParticipantUpdate`);
    //code
  }

  
  console.log("(200 OK!)");
  res.sendStatus(200);
});
 

var ngrokOptions = {
  proto: 'http',
  addr: config.port
};

if (config.ngrokSubdomain) {
  ngrokOptions.subdomain = config.ngrokSubdomain
}

ngrok.connect(ngrokOptions).then(url => {
  console.log('ngrok url is ' + url);
}).catch(console.error);
