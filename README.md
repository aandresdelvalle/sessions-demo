This application is designed for demonstrating the capabilities of the Messaging Sessions API, particularly how Chat users in the browser can exchange messages seamlessly with others on SMS or WhatsApp.

## Configuring and getting started.

This demo requires a Twilio account. You'll need to collect some credentials from the [Twilio Console](twilio.com/console):

 - **Your Account SID** A string in the form `ACxx`, accessible from the [Dashboard](twilio.com/console/dashboard)

 - **An API/Signing Key Pair** These are an `SKxx` sid and the accompanying secret, part of the [Runtime](https://www.twilio.com/console/runtime/api-keys). A standard key is fine.

 - **Your Account's Chat Service SID** An `ISxx` SID which is best determined by creating a new session.

Creating a new session is straightforward. Check out the [code samples](https://www.twilio.com/docs/sms/conversational-messaging-sessions/sessions-resource?code-sample=code-create-session&code-language=curl&code-sdk-version=json) or run something like this from your command line:

    curl -X POST \
        https://conversations.twilio.com/v1/Conversations \
        --user SKxx:secret
        -d MessagingServiceSid=MGxx

In the response, look for `service_sid`; this is the value you want. Additionally, save the value in `sid` -- this will come in handy later on.

### Create a .env file.

Clone this repository. In the working directory, copy the `.env.example` file to a new file called `.env`. Inject the information you gathered above into the appropriate fields. It should look something like this:

    TWILIO_ACCOUNT_SID=

    TWILIO_AUTH_TOKEN=

    TWILIO_API_KEY=

    TWILIO_API_SECRET=

    TWILIO_CHAT_SERVICE_SID=

    TWILIO_MESSAGING_SERVICE_SID=

### Upgrade node.js and install dependencies

Your system may not have a particularly recent version of node.js installed by default. This demo requires Node.js v10 or higher. Use the package manager most appropriate on your system. On Mac OS, you probably want `brew upgrade node.js`.

Test this by running `npm install`. This should execute successfully to completion.

### Set your Twilio Messaging service to create a new Conversation

Select the option to create a new Conversation per number messaging the service.

![Check option to create conversations](./one.png)

Configure conversations to be able to add the Support participant each time a Conversation is Added.

Add the message service

![add the message service](./two.png)

Add the webhooks.

Remember to use the same URL as the token server's as it is where the API service to automatically add Support as a participant runs - in my code is ngrok subdomain -support-

![conversations webhooks](./webhooks.png)

### Start the Demo

Run `npm start`. This will automatically start two processes:

 1. One starts a token server, using the credentials from your `.env` file.
 2. The other runs a react app. Your browser should open automatically to https://localhost:3000/

We can also  the servers separately:

 1. `npm run start-token-server` will start the token server - This is currently using the ngrok subdomain support -  
 2. `npm run start-chat-app` will start the webchat

### Add the Support user

When you log with the ID Support - you get access to the chat interface that receives all the mensages, separated by channel. One per SMS/Whatsapp request.


### Add a Chat Participant

If you log as participant (not Support)  using another browser or incognito you can add a chat channel to support.

### Add an SMS Participant

Chatting with yourself isn't as satisfying without the comforting pling of SMS arrival. Visit the [Sessions SMS quickstart](https://twilio.com/docs/sms/conversational-messaging-sessions) to create a phone number. When you finally start adding participants, use the same session SID you used above to get started.

If all goes well, you'll have bridged an SMS user with a Chat user, all in a few requests!


### Add a whatsapp Participant (the process works the same as for SMS)

Add a Whatsapp number to the Messanging Service. 

You will need to have your own  Whatsapp enabled number as the sandbox number cannot be added to a messaging service.

You can find the process to get a Whatsapp enabled number here: https://www.twilio.com/docs/whatsapp/tutorial/connect-number-business-profile

## Disclaimer

 Be advised that Twilio Conversations is under active development and the APIs you use here may change.
